#include "BSNode.h"
BSNode::BSNode(string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}
//copy constractor
BSNode::BSNode(const BSNode& other)
{
	this->_left = other._left;
	this->_right = other._right;
	this->_count = other._count;
	this->_data = other._data;
}
BSNode::~BSNode()
{
	if (this->getLeft() != nullptr) this->getLeft()->~BSNode();
	if (this->getRight() != nullptr) this->getRight()->~BSNode();
	// we check about our left and rights, if they have sons we will call thier destructor
}
void BSNode::insert(string value)
{
	if (this->getData() == value) // if we have the value already in
		this->_count++;
	else if (value < this->getData())
	{
		if (this->getLeft() != nullptr)
			this->getLeft()->insert(value);
		else
			this->_left = new BSNode(value);
	}
	else if (value > this->getData())
	{
		if (this->getRight() != nullptr)
			this->getRight()->insert(value);
		else
			this->_right = new BSNode(value);
	}

}
BSNode& BSNode::operator=(const BSNode& other)
{
	// we have already a copy constructor so we create a new bsnode and then we return its refrence
	return *(new BSNode(other));
}
bool BSNode::isLeaf() const
{
	return (this->getLeft() == nullptr && this->getRight() == nullptr);
}
string BSNode::getData() const
{
	return this->_data;
}
BSNode* BSNode::getLeft() const
{
	return this->_left;
}
BSNode* BSNode::getRight() const
{
	return this->_right;
}
bool BSNode::search(string val) const
{
	//recursive search
	if (this->getData() == val) // stop condition if the data is the value we return true
		return true;
	else if (this->isLeaf()) // stop condition - if we got to the last node and its a leaf return false
		return false;
	else
		return val < this->getData() ? this->getLeft()->search(val) : this->getRight()->search(val);
	//if the value is smaller then the current value we need to check the left subtree so we search on the left tree
	//if the value is bigger then the current value we need to check the right subtree so we search on the right tree
}
int BSNode::getHeight() const
{
	//you check if the tree has something in each side
	//of so we recurisve till the end of the tree
	// and add to counter
	//then check which side is bigger
	int counterLeft = 1, counterRight = 1;
	if (this->getLeft() != nullptr)
	{
		counterLeft++;
		this->_left->getHeight();
	}
	if (this->getRight() != nullptr)
	{
		counterRight++;
		this->_right->getHeight();
	}
	return counterLeft > counterRight ? counterLeft : counterRight;
}
int BSNode::getDepth(const BSNode& root) const
{
	
	if (!root.search(this->getData())) // if the root doesnt exit thats an error
		return -1;
	return this->getCurrNodeDistFromInputNode(&root);
	
}
int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	if (node == nullptr)
		return 0;
	if (node->getData() == this->getData())
		return 1;
	return 1 + std::max(this->getCurrNodeDistFromInputNode(node->getLeft()), this->getCurrNodeDistFromInputNode(node->getRight()));

}
void BSNode::printNodes() const
{
	if (this->getLeft() != nullptr) this->getLeft()->printNodes();
	std::cout << "Data = " << this->_data << " Showed times : " << this->_count << '\n';
	if (this->getRight() != nullptr) this->getRight()->printNodes();
}