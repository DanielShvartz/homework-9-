#pragma comment(lib, "printTreeToFile.lib")
#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"
using namespace std;


int main()
{
	BSNode* bs = new BSNode("8");
	bs->insert("3");
	bs->insert("1");
	bs->insert("6");
	bs->insert("10");
	bs->insert("8");
	bs->insert("3");
	bs->insert("1");
	bs->insert("6");
	bs->insert("10");

	
	cout << "Tree height: " << bs->getHeight() << endl;
	//THIS LINE DOESNT WORK  << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;
	bs->printNodes();
	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;
	return 0;
}

