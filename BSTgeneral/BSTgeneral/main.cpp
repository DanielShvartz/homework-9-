#include "BSNode.h"

int main()
{
	int i;
	int arrInt[15] = { 6,5,3,8,2,6,7,1,9,0,5,12,34,4,11 }; // created array
	BSNode<int>* treeInt = new BSNode<int>(arrInt[0]); // gave first
	for (i = 1; i < 15; i++) // entered others
	{
		std::cout << arrInt[i] << " ";
		treeInt->insert(arrInt[i]);
	}
	std::cout << endl;
	treeInt->printNodes(); // print fixed
	char arrChar[15] = { 'f','h','w','a','c','d','s','g','z','x','w','k','u','b','!' };
	BSNode<char>* treeChar = new BSNode<char>(arrChar[0]);
	for (i = 1; i < 15; i++)
	{
		std::cout << arrChar[i] << " ";
		treeChar->insert(arrChar[i]);
	}
	treeChar->printNodes();
	getchar();


}

