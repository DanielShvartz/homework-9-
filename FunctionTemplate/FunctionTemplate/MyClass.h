#pragma once
#include <iostream>
class MyClass
{
public:
	MyClass(char m = 'w');
	char member;
	~MyClass();
	bool operator > (MyClass other);
	bool operator == (MyClass other);
	friend std::ostream &operator<<(std::ostream &output, const MyClass &D);
};

