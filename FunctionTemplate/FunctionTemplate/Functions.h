#pragma once
#include <iostream>
template <class T>
int compare(T first, T second)
{
	if (first > second) // very simple
		return -1;
	else if (second > first)
		return 1;
	else
		return 0;
}
template <class T>
void bubbleSort(T arr[], int size)
{
	bool check = false;
	T temp;
	for (int i = 0; i < size - 1; i++)
	{
		if (compare<T>(arr[i], arr[i + 1]) == -1) // we call compare to check between if the first is bigger
		{
			check = true;
			temp = arr[i]; // swap
			arr[i] = arr[i + 1];
			arr[i + 1] = temp;
		}
	}
	if (check) bubbleSort<T>(arr, size); //recursive
}
template <class T>
void printArray(T arr[], int size)
{
	for (int i = 0; i < size; i++)
		std::cout << arr[i] << std::endl;
}