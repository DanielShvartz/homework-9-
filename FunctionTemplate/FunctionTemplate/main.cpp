#include "functions.h"
#include <iostream>
#include "MyClass.h"

int main() {

//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	std::cout << "----------------------------------------------------"<<std::endl;
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('b', 'o') << std::endl;
	std::cout << compare<char>('r', 'b') << std::endl;
	std::cout << compare<char>('w', 'w') << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	char charArr[arr_size] = { 'n', 'o', 'd', 'e', 'r' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;
	std::cout << "----------------------------------------------------" << std::endl;

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<MyClass>(MyClass('d'), MyClass('o')) << std::endl;
	std::cout << compare<MyClass>(MyClass('r'), MyClass('b')) << std::endl;
	std::cout << compare<MyClass>(MyClass('w'), MyClass('w')) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	MyClass classArr[arr_size] = { MyClass('n'), MyClass('o'), MyClass('d'), MyClass('e'), MyClass('r') };
	bubbleSort<MyClass>(classArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << classArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<MyClass>(classArr, arr_size);
	std::cout << std::endl;
	std::cout << "----------------------------------------------------" << std::endl;


	system("pause");
	return 1;
}