#include "MyClass.h"
MyClass::MyClass(char m)
{
	this->member = m;
}

MyClass::~MyClass()
{
}
bool MyClass::operator > (MyClass other)
{
	return this->member > other.member;
}
bool MyClass::operator == (MyClass other)
{
	return this->member == other.member;
}
std::ostream &operator<<(std::ostream &output, const MyClass &D)
{
	output << D.member; 
	return output;
}